import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ['input', 'alertBox', 'alertTitle', 'alertBody']

  handleSuccess(event) {
    const sentTo = event.detail[0].to;
    const message = event.detail[0].message;
    this.updateAlertTitle('Text sent!', 'has-text-success');
    this.alertBodyTarget.innerHTML = this.successHTML(sentTo, message);
    this.showAlertBox();
    this.clearInputs();
  }

  handleError(event) {
    const message = event.detail[0].message;
    this.updateAlertTitle(message, 'has-text-danger');
    this.alertBodyTarget.innerHTML = this.errorHTML();
    this.showAlertBox();
  }

  updateAlertTitle(text, cssClass) {
    this.alertTitleTarget.innerText = text;
    const oppositeClass = cssClass === 'has-text-success' ? 'has-text-danger' : 'has-text-success'
    this.alertTitleTarget.classList.remove(oppositeClass);
    this.alertTitleTarget.classList.add(cssClass);
  }

  showAlertBox(el) {
    this.alertBoxTarget.classList.remove('is-invisible');
  }

  clearInputs() {
    this.inputTargets.forEach((el) => el.value = '');
  }

  successHTML(sentTo, message) {
    return(
      '<p>' +
        `<strong>To: </strong><span>${sentTo}</span>` +
      '</p>' +
      '<p>' +
        `<strong>Message: </strong><span>${message}</span>` +
      '</p>'
    )
  }

  errorHTML() {
    return(
      '<p>Check the artist and phone number and try again.</p>'
    )
  }
}
