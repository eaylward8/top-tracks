import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ['container', 'image', 'name', 'followers', 'releaseImage', 'releaseName', 'releaseDate',
                    'releaseType', 'chartContainer']

  populate(event) {
    const artistInfo = event.detail[0].artist_info;
    this.imageTarget.src = artistInfo.image_url;
    this.nameTarget.innerText = artistInfo.name;
    this.followersTarget.innerText = artistInfo.followers;

    this.releaseImageTarget.src = artistInfo.latest_release.image_url;
    this.releaseNameTarget.innerText = artistInfo.latest_release.name;
    this.releaseDateTarget.innerText = artistInfo.latest_release.release_date;
    this.releaseTypeTarget.innerText = artistInfo.latest_release.album_type;

    this.updateChartData(artistInfo.related_artist_popularity);
    this.showContainer();
  }

  updateChartData(data) {
    Chartkick.charts['popularity-chart'].updateData(data);
  }

  showContainer() {
    this.containerTarget.classList.remove('is-invisible');
  }

  hide() {
    this.containerTarget.classList.add('is-invisible');
  }
}
