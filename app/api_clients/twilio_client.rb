# frozen_string_literal: true

class TwilioClient
  TEST_ACCT_ID    = Rails.application.credentials.twilio[:test_account_sid]
  TEST_AUTH_TOKEN = Rails.application.credentials.twilio[:test_auth_token]
  TEST_FROM_NUM   = '+15005550006'

  attr_reader :message

  def initialize
    @client = Twilio::REST::Client.new(TEST_ACCT_ID, TEST_AUTH_TOKEN)
  end

  def send_text(to, body)
    # TODO: phone number validation; check if body is blank
    # TODO: rescue from Twilio::REST::RestError ?
    @message = @client.messages.create(from: TEST_FROM_NUM, to: to, body: body)
  end
end
