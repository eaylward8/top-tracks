# frozen_string_literal: true

class SpotifyClient
  CLIENT_ID     = Rails.application.credentials.spotify[:client_id]
  CLIENT_SECRET = Rails.application.credentials.spotify[:client_secret]

  include ActionView::Helpers::NumberHelper

  attr_reader :artist, :top_track

  def initialize(artist_name)
    RSpotify.authenticate(CLIENT_ID, CLIENT_SECRET) # TODO: error handling if auth fails?
    @artist = RSpotify::Artist.search(artist_name).first
    raise unless @artist # TODO: implement custom error class

    @top_track = @artist.top_tracks(:US).first
  end

  def artist_info
    {
      name: @artist.name,
      followers: number_with_delimiter(@artist.followers['total']),
      image_url: @artist.images.first&.fetch('url'),
      latest_release: latest_release_info,
      related_artist_popularity: related_artist_popularity
    }
  end

  private

  def latest_release_info
    latest_release = @artist.albums(album_type: 'album,single', market: :US).max_by(&:release_date)
    {
      name: latest_release.name.truncate(30),
      album_type: latest_release.album_type.capitalize,
      release_date: Date.strptime(latest_release.release_date, '%Y-%m-%d').strftime('%b %-d, %Y'),
      image_url: latest_release.images.first&.fetch('url')
    }
  end

  def related_artist_popularity
    arr = @artist.related_artists.first(9).map { |artist| [artist.name, artist.popularity] }
    arr.unshift([@artist.name, @artist.popularity])
  end
end
