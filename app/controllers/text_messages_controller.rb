# frozen_string_literal: true

class TextMessagesController < ApplicationController
  # rescue_from StandardError, with: :render_error # TODO: rescue from more specific errors

  def create
    spotify_client = SpotifyClient.new(params[:artist])
    body = "#{spotify_client.artist.name}'s top track: #{spotify_client.top_track.name}"
    message = send_message(body)
    render json: { to: message.to, message: message.body, artist_info: spotify_client.artist_info }
  end

  private

  # def text_message_params
  #   binding.pry
  #   params.permit(:artist, :phone_number)
  # end

  def send_message(body)
    twilio_client = TwilioClient.new
    twilio_client.send_text(params[:phone_number], body)
  end

  def render_error
    render json: { message: 'Woops. Something went wrong.' }, status: :internal_server_error
  end
end
