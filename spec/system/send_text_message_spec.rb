# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Sending top track text', type: :system do
  let(:spotify_client) { SpotifyClient.new('Wu-Tang Clan') }
  let(:top_track)      { spotify_client.top_track.name }
  let(:followers)      { number_with_delimiter(spotify_client.artist.followers['total']) }

  before :each do
    visit root_path
  end

  context 'with valid artist and phone number' do
    scenario 'sends text and displays artist info', :js do
      expect(find('#artist-container', visible: :all).visible?).to be false

      fill_in 'Artist', with: 'Wu-Tang Clan'
      fill_in 'Phone number', with: '647-445-1141'
      click_on 'Send Top Track'

      within '.hero-body' do
        expect(page).to have_content 'Text sent!'
        expect(page).to have_content '6474451141'
        expect(page).to have_content "Wu-Tang Clan's top track: #{top_track}"
      end

      expect(find('#artist-container').visible?).to be true

      within '#artist-info-tile' do
        expect(page).to have_content 'Wu-Tang Clan'
        expect(page).to have_content "Followers: #{followers}"
      end

      within '#popularity-tile' do
        expect(page).to have_content 'Related Artist Popularity'
        expect(page).to have_css '#popularity-chart'
      end
    end
  end

  context 'with invalid artist and valid phone number' do
    scenario 'displays error message', :js do
      fill_in 'Artist', with: 'asdlkjfnh34y'
      fill_in 'Phone number', with: '647-445-1141'
      click_on 'Send Top Track'

      within '.hero-body' do
        expect(page).to have_content 'Woops. Something went wrong.'
        expect(page).to have_content 'Check the artist and phone number and try again'
      end

      expect(find('#artist-container', visible: :all).visible?).to be false
    end
  end

  context 'with valid artist and invalid phone number' do
    scenario 'displays error message', :js do
      fill_in 'Artist', with: 'A Tribe Called Quest'
      # https://www.twilio.com/docs/iam/test-credentials#test-incoming-phone-numbers
      fill_in 'Phone number', with: '15005550001'
      click_on 'Send Top Track'

      within '.hero-body' do
        expect(page).to have_content 'Woops. Something went wrong.'
        expect(page).to have_content 'Check the artist and phone number and try again'
      end

      expect(find('#artist-container', visible: :all).visible?).to be false
    end
  end
end
