# Top Tracks

Demo application that lets you search for an artist, enter a phone number, and receive that artist's top Spotify track via text message.

The app also displays information about the artist from Spotify, including followers, latest release, and popularity compared to similar artists.

## Technology
* Rails 5.2
* StimulusJS (https://stimulusjs.org/)
* Bulma CSS Framework (https://bulma.io/)
* Spotify API
* Twilio API