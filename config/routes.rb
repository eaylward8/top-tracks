# frozen_string_literal: true

Rails.application.routes.draw do
  get 'welcome/index'
  resources :text_messages, only: :create

  root 'welcome#index'
end
